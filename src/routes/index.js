const routes = require('express').Router();
import exchangeRoutes from './exchange';

routes.use('/exchange', exchangeRoutes);

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});

module.exports = routes;