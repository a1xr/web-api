const routes = require('express').Router();
import {ExchangeServices, UniverseService} from 'a1xr-feed-manager';

const getExchange = function(){
    var exchangeCode = ExchangeServices.ExchangeCodes.BINANCE_CODE;
    return ExchangeServices.GetExchange(exchangeCode);
};

routes.get('/assetlookup/', async (req, res) => {

    var data = await UniverseService.getEntities();
    res.status(200).json(data);
});

routes.get('/exchangeinfo/', async (req, res) => {
    res.status(200).json(await getExchange().exchangeInfo());
});

routes.get('/accountinfo/', async (req, res) => {
    res.status(200).json(await getExchange().accountInfo());
});

routes.get('/orders/open/:symbol/', async (req, res) => {
    res.status(200).json(await getExchange().openOrders({
        symbol : req.params.symbol,
        timestamp : new Date().getTime()
    }));
});

routes.get('/prices/:symbol/', async (req, res) => {
    res.status(200).json(await getExchange().prices({
        symbol : req.params.symbol
    }));
});

routes.get('/book/:symbol/', async (req, res) => {
    res.status(200).json(await getExchange().bookTicker({
        symbol : req.params.symbol
    }));
});



routes.get('/candles/:symbol/:interval/', async (req, res) => {
    
    var data = await getExchange().candles({
        symbol : req.params.symbol,
        interval : req.params.interval,
        limit : 1000
    });
    
    res.status(200).json(data);
});
  
module.exports = routes;
