"use strict";

var _exchange = _interopRequireDefault(require("./exchange"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = require('express').Router();

routes.use('/exchange', _exchange["default"]);
routes.get('/', function (req, res) {
  res.status(200).json({
    message: 'Connected!'
  });
});
module.exports = routes;